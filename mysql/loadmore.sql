-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 28, 2020 at 10:35 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `loadmore`
--
CREATE DATABASE IF NOT EXISTS `loadmore` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `loadmore`;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `date_published` datetime DEFAULT CURRENT_TIMESTAMP,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `date_published`, `content`) VALUES
(1, '2020-02-28 08:01:38', 'Lucas ipsum dolor sit amet luke monkey-lizard skywalker auril fortuna bail daala arvel ansuroer saleucami. Joelle dash kowakian duro biggs shi\'ido kasan. Huk shaddaa hallotan katarn fortuna. Garm typho skywalker massassi saurin jabba antemeridian watto. M'),
(2, '2020-02-28 08:03:20', 'Watto skywalker ahsoka aqualish fett voxyn zannah raa. Jarael auril talortai bothan kitonak kendal hobbie organa. Sly mod amidala evazan rakata. Neimoidian mantell vivenda mara gev aayla elrood greeata. Tierce rebo quence shaddaa terrik corran. Tion iego '),
(3, '2020-02-28 08:03:20', 'Jabba trioculus noghri mirialan sulorine asajj cody calrissian carnor. Sykes windu bib coruscant karrde gotal mygeeto gricho juvex. Aqualish roos organa ysanne wuher ozzel. Raioballo zeltron jusik vaathkree. Xizor su anakin kubaz ozzel stele. Stereb darpa'),
(4, '2020-02-28 08:04:17', 'Rom veknoid maris coruscant borvo. Sith fortuna greeata boltrunians annoo. Nute soontir boba mothma. Secura wicket selonian irek garm jacen. Utapau mothma katarn joruus kobok. Chistori tarkin kit nar sern sluis dormé. Rom cerean droopy borsk sesswenna woostoid. Tatooine corellia shistavanen vaathkree darth jin\'ha barriss hutt gank. Ahsoka rune timoliini toydarian gorith. Skirata jax farlax valorum kathol utapau. Cognus hissa terentatek shimrra nass tibor. Gunray briqualon dor hutta paaerduag whitesun.\r\n\r\n'),
(5, '2020-02-28 08:04:17', 'Lah duro tarkin mohc kwa. Oppo rebo aayla bith ben senex. Thrackan panaka utai ahsoka yaka depa mon ismaren. Subterrel kota qui-gon watto. Adi jettster dat r2-d2 mustafar cato. Yoda chistori hypori darth darth neimoidian. Haruun natasi kast iblis. Secura rotta vilim metalorn owen d8 quinlan ventress dormé. Ysanne jerec frozian yané kanos. Organa subterrel owen subterrel. Fel dressellian hobbie mon baba lama ponda. Jabiimas weequay jarael tsavong iego wat porkins. Nar cordé veila max. Ailyn rune momaw gank mas mirialan.\r\n\r\n'),
(6, '2020-02-28 08:04:36', 'Warrick haako su jinn nomi endocott tython. Hssis drach\'nam bothawui talz codru-ji colton gado kit ord. Deliah droopy draethos ismaren gizka yevetha porkins. Gonk chewbacca windu ishi lando amanin. Jagged di joruus san antilles lumiya mothma piell. Adi iblis kessel oola keshiri jabba lobot xizor. Croke saesee pau\'an droopy moff salacious darth geonosian ssi-ruuk. Nar seerdon saché ig-88 wat. Saleucami lars thisspias yuuzhan meridian kendal vestara. Marek bib droch antilles. Windu nagai phlog troig k-3po rotta watto caedus.\r\n\r\n'),
(7, '2020-02-28 08:04:36', 'Unduli lorth fortuna ithorian darklighter zabrak. Bail selonian cal x2 coruscant illum sola. Juvex cassio koon darth spar sulorine moff sesswenna. Grodin quee haruun mara yevetha jubnuk qel-droma kashyyyk nelvaanian. Ikrit dashade vao maximilian darth yavin rhen stass. Skywalker var solo hutt gilad myneyrsh ysanne gado gonk. Geonosian desolous durron irek stereb. Karrde md-5 argazdan amidala han falleen finis skywalker. R5-d4 ben frozarns jek valorum geonosis jusik. Sabé zuckuss derek qu arvel.\r\n\r\n'),
(8, '2020-02-28 08:04:54', 'Nute tyranus snootles farlax utapaun fey\'lya nien dexter. Kowakian ken cliegg saffa hutt asajj farlax mon desolous. Chistori seerdon shi\'ido defel. Ferroans kal ev-9d9 iridonian cornelius neti raynar. Rom sio r5-d4 kiffar kal drall haruun cerea frozarns. Endor maximilian bibble t88 lars utapau vau valorum. -1b abyssin roonan chazrach darth. Djo triclops castell rex. Illum olié mace epicanthix endor lyn klaatu kitonak. Arcona mandell wuher wedge dexter antemeridian katarn gamorrean. Bib kuat disra san braxant.\r\n\r\n'),
(9, '2020-02-28 08:04:54', 'Nikto gwurran rukh aqualish. Jettster solo moddell metalorn mccool anomid mustafar. Fey\'lya atrivis tyber iv zann. Orrin vagaari tion raymus umbaran joelle. Sikan wilhuff gunray reach dashade bib unu falleen. Ysanne ken vima-da-boda noghri. Tib soontir naberrie desann sabé terentatek yoda leia. Pavan elom trioculus lorth kashyyyk jango maximilian. Kyp tono hapes natasi talz mara luuke momaw utai. Borvo cliegg darth rattataki abyssin saleucami ti besalisk raioballo. Cerean choka lannik stass. Darth vos hissa rex zuggs yuuzhan jabba.\r\n\r\n'),
(10, '2020-02-28 08:05:04', 'Oswaft chazrach jade monkey-lizard kal moff sluis. Bria doldur nute abyssin coruscant oppo veila. Vong skywalker yoda geonosian windu jan muunilinst nautolan medon. Terentatek cato roonan caedus organa. Rune organa moff iv askajian mandell yuzzum oppo. Organa clawdite falleen oola thistleborn nahdar lando rahm tarpals. Grodin feeorin gado jax dantooine ewok bren plo. Mohc togruta nomi squib nien raioballo gorax tharin gunray. Kalarba aleena calrissian kiffar organa tagge colton. Kiffar shmi shaak triclops.\r\n\r\n');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
