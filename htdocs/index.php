<?php
include('config.php');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Twitter Style load more results.</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/
libs/jquery/1.3.0/jquery.min.js"></script>

<script type="text/javascript">
$(function() {
	//More Button
	$('.more').live("click",function() 
	{
		$('.morebox').remove();
		
		var ID = $(this).attr("id");
		if(ID)
		{
			$("#more"+ID).html('<img src="moreajax.gif" />');

			$.ajax({
				type: "POST",
				url: "ajax_more.php",
				data: "lastmsg="+ ID, 
				cache: false,
				success: function(html){
					$("ol#updates").append(html);
					$("#more"+ID).remove();
				}
			});
		//

		//				
		}

		return false;
	});
});

</script>

</head>

<body>

<div id='container'>
<ol class="timeline" id="updates">
	<?php
	$query = $db->query("SELECT * from `posts` ORDER BY `id` ASC LIMIT 2");
	$result = $query->num_rows;
	while($row=$query->fetch_assoc()){
		$msg_id = $row['id'];
		$date = date('d/m/Y', strtotime($row['date_published']));
		$message = $row['content'];
	?>
	<li>
		<?php echo $date.' - '.$message; ?>
	</li>
	<?php 
	} 
	?>

</ol>
<?php if($result>0){ ?>
<div id="more<?php echo $msg_id; ?>" class="morebox">
<a href="#<?php echo $msg_id; ?>" class="more" id="<?php echo $msg_id; ?>">more</a>
</div>
<?php } ?>
</div>

</body>
</html>
